<?php

namespace App;

class WebRequest{
	private $url;
	private $conn;
	private $settings;
	private $response;

	public function __construct($url){
		$this->url = $url;
		$this->conn = curl_init();

		$this->settings = array(
			CURLOPT_URL => $this->url,
			CURLOPT_FRESH_CONNECT => true,
			CURLOPT_CONNECTTIMEOUT_MS => 3000,
			CURLOPT_TIMEOUT_MS => 3000,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($this->conn, $this->settings);
	}

	public function response(){
		$this->response = curl_exec($this->conn);
		return $this->response;
	}
}
