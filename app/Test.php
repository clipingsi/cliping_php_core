<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class test extends Eloquent{
    public $table = "tests";
    public $timestamps = false;
}
