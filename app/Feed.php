<?php

namespace App;

use App\Article;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Feed extends Eloquent{
    public $table = "feeds";
    public $timestamps = false;
    
    public function getFeedData(){
    	$request = new WebRequest($this->url);
    	return $request->response();
    }

    public function getArticles($recent_count = 50){
    	return Article::where('source_feed_id',$this->id)->orderBy('id','DESC')->limit($recent_count)->get();
    }
}
