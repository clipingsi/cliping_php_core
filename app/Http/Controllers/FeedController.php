<?php

namespace App\Http\Controllers;

use App\Test;
use App\Feed;
use App\Article;
use App\WebRequest;

include_once app_path().'/Includes/simple_html_dom.php';

use DOMDocument;
use DOMXPath;

use Response;
use Illuminate\Http\Request;

class FeedController extends Controller
{

	public function getFeeds(){
		$feeds = Feed::get();
		return response(json_encode($feeds),200)->header('Content-Type','application/json');
	}

	public function parseFeed($feed_id){
		echo 'hello<br>';
		$feed = Feed::find($feed_id);
		if(!isset($feed) || empty($feed)){
			return response('Feed not found',404);
		}

		$nextParseAt = date('Y-m-d H:i:s',strtotime($feed->parsed_at)+$feed->ttl);

		if($nextParseAt<=date('Y-m-d H:i:s')){
			$xml = simplexml_load_string($feed->getFeedData());
			$rss_channel = $xml->channel;

			$feed->changed_at = date('Y-m-d H:i:s',strtotime($rss_channel->lastBuildDate));
			$feed->save();

			foreach ($rss_channel->item as $item) {
				$past_article = Article::where('url',$item->link)->first();
				if(empty($past_article)){
					$split_expression = "/( |\(|\)|[-]|[:]|[.]|[,]|[!]|[?]|[\"])/";
					$title_tags = preg_replace($split_expression,'_',strtolower($item->title));
					while(strpos($title_tags, '__')){
						$title_tags = preg_replace('/__/','_',strtolower($title_tags));
					}

					$description = $item->description;
					$htmlEndPos = strpos($description,'/>');
					if($htmlEndPos!==false){
						$description = ltrim(rtrim(substr($description,$htmlEndPos+2)));
					}
				
					$article_id = Article::insertGetId([
						'source_feed_id' => $feed->id,
						'title' => $item->title,
						'description' => $description,
						'url' => $item->link,
						'keywords' => $title_tags,
						'article_timestamp' => date('Y-m-d H:i:s',strtotime($item->pubDate))
					]);
				}
			}
			$feed->parsed_at = date('Y-m-d H:i:s');
			$feed->save();
		}
		return response('OK',200);
	}

	public function test(){
		/*
		$articles = Article::get();
		foreach ($articles as $article) {
			$split_expression = "/( |\(|\)|[-]|[:]|[.]|[,]|[!]|[?]|[\"])/";
			$txt = preg_replace($split_expression,'_',strtolower($article->title));
			while(strpos($txt, '__')){
				$txt = preg_replace('/__/','_',strtolower($txt));
			}
			$article->keywords = $txt;
			$article->save();
		}
		echo 'done';
		*/
	}

}
