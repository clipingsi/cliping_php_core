<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Article extends Eloquent{
    public $table = "articles";
    public $timestamps = false;
    
}
