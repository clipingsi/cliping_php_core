<?php

namespace App\Console\Commands;

use App\Feed;
use Illuminate\Console\Command;

class ParseFeedsAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse_feed_all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses all active feeds for new articles.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO parse all feeds not just one
        $feeds = Feed::get();
        foreach ($feeds as $feed) {
            app('App\Http\Controllers\FeedController')->parseFeed($feed->id);
        }
    }
}
