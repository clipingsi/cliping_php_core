<!DOCTYPE html>
<html>
<head>
	<title>Cliping - Feed</title>
</head>
<body>
	<?php
		use App\Feed;
		use App\Article;

		$feed = Feed::find($feed_id);
		if(!empty($feed)){
			
			$articles = $feed->getArticles()->sortByDesc('article_timestamp');
			if(count($articles)>0){
				echo '<table>
					<tr>
						<th>Title</th>
						<th>Published on</th>
					</tr>';
				foreach ($articles as $article) {
					echo '<tr><td><a target="_blank" href="'.$article->url.'">'.$article->title.'</a></td><td>'.date('H:i d.m.Y',strtotime($article->article_timestamp)).'</td></tr>';
				}
				echo '</table>';
			}else{
				echo 'No articles in feed...';
			}
		}else{
			echo 'Ooops! Somebody stole christmas! Was is Grinch?';
		}
	?>
</body>
</html>